﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 托普利茨矩阵
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int row, clo;//矩阵的行和宽
            Console.WriteLine("请输入矩阵的行和列数：");
            row = Convert.ToInt32(Console.ReadLine());
            clo = Convert.ToInt32(Console.ReadLine());
            int[,] matrix = new int[row, clo];
            Console.WriteLine("请输入矩阵的数据，先行后列");
            for(int i = 0; i < row; i++)
            {
                int temp = 0;
                for(int j = 0; j < clo; j++)
                {
                    temp = Convert.ToInt32(Console.ReadLine());
                    matrix[i, j] = temp;
                }
            }
            Console.WriteLine(Judge(matrix));

        }
        private static bool Judge(int[,] a)
        {
            int row = a.GetLength(0);
            int clo = a.GetLength(1);
            for(int i = 0; i < row-1; i++)
            {
                for(int j = 0; j < clo - 1; j++)
                {
                    if (a[i,j] != a[i + 1,j + 1])
                    {
                        return false;
                    }
                } 
                
            }
            return true;

        }
    }
}
