﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prime
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("请输入一个整数：");
            string num = Console.ReadLine();
            int number = Convert.ToInt32(num);//输入数据
            Console.WriteLine("整数" + number + "的质因数有：");
           
            
            for(int i = 2; i <= number; i++)
            {
                
                while (number % i == 0)
                {
                    Console.WriteLine(i + " ");
                    number /= i;//短除法求因数
                }
            }
        }
    }
}
