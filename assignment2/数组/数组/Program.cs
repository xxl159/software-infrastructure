﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 数组
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入数组的长度");
            string leng = Console.ReadLine();
            int length = Convert.ToInt32(leng);
            int[] array = new int[length];
            Console.WriteLine("请输入" + length + "个整数");
            
            //构建一个整数
            for(int i = 0; i < length; i++)
            {
                int number;//暂时存储数组中的数据
                string temp = Console.ReadLine();
                try
                {
                    number = Convert.ToInt32(temp);
                    array[i] = number;
                }catch(Exception e)
                {
                    Console.WriteLine("请输入整数"+e.Message);
                }

            }
            Console.WriteLine("整数数组的最大值为" + MaxNumber(array));
            Console.WriteLine("最小值为" + MinNumber(array));
            Console.WriteLine("平均值为" + Average(array));
            Console.WriteLine("元素的和为" + Sum(array));


        }
        //最大值
        private static int MaxNumber(int[] a)
        {
            int max = 0;
            for(int i = 1; i < a.Length; i++)
            {
                if (a[i] > max)
                {
                    max = a[i];
                } 
            }
            return max; 
        }
        //最小值
        private static int MinNumber(int[] a)
        {
            int min = a[0];
            for(int i = 1; i < a.Length; i++)
            {
                if (a[i] < min)
                {
                    min = a[i];
                }
            }
            return min;
        }

        private static int Sum(int[] a)
        {
            int sum = 0;
            for(int i = 0; i < a.Length; i++)
            {
                sum += a[i];
            }
            return sum;
        }

        private static int Average(int[] a)
        {
            int sum = 0;
            int average = 0;
            for (int i = 0; i < a.Length; i++)
            {
                sum += a[i];
            }
            average = sum / a.Length;
            return average;
        }
    }
}
