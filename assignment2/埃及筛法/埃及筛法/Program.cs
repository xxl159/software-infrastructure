﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 埃及筛法
{
    internal class Program
    {
        static void Main(string[] args)
        {   
            //把数据放入一个数组中
            int[] array=new int[201];
            array[0] = 0;//0不是是素数
            array[1] = 0;
            for(int i = 2; i < 201; i++)
            {
                array[i] = i;
            }

            int[] result = new int[50];
            int cnt = 0;
            for(int i = 2; i < 201; i++)
            {
                if (array[i] != 0)
                {
                    result[cnt++] = i;
                    for(int j = i * i; j <= 200; j += i)
                    {
                        array[j] = 0;//合数置零
                    }
                }
            }
            int m = 0;
            while (result[m] != 0)
            {
                Console.Write(result[m]+" ");
                m++;
            }
                
        }
    }
}
