﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace 订单管理
{
    internal class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class Order
    {
        private string orderID;
        private string address;
        private double totalPrice;
        private Client client;
        private List<OrderItem> items;

      
        public string OrderId
        {
            get;set;
        }
        public string Address 
        {
            get; set; 
        }
        public double TotalPrice
        {
            get;set;
        }
        public List<OrderItem> Items
        {
            get;set;
        }
        public Client Client { get; set; }

        public Order() {  }
        public Order(string id,string addr,Client c,List<OrderItem> items )
        {
            orderID = id;
            address = addr;
            client = c;
            this.items = items;
            totalPrice = 0;
            foreach(OrderItem item in items)
            {
                totalPrice += item.ItemPrice;
            }
        }

        public override bool Equals(object obj)
        {
            Order o = obj as Order;
            return o != null && orderID != o.orderID;
        }

        public override string ToString()
        {
            string s = "订单ID" + orderID + "  订单地址：" + address + "  客户名：" + client + "  商品总价：" + totalPrice;
            foreach(OrderItem item in items)
            {
                s += item + " ";
            }
            return s;
        }

        public int CompareTo(object obj)
        {
            Order o = obj as Order;
            if (o != null)
            {
                int id1 = Convert.ToInt32(o.orderID);
                int id2 = Convert.ToInt32(this.orderID);
                return id1 - id2;
            }
            throw new NotImplementedException();
        }

    }


    public class OrderItem
    {
        private string id;
        private int num;
        private double itemPrice;
        private Product p;

        public string Id { get; }
        public Product P { get; set; }
        public double ItemPrice { get; set; }
        public int Num { get; set; }

        public OrderItem() { }

        public OrderItem(string id, int num , Product p)
        {
            this.id = id;
            this.num = num;
            this.p = p;
            this.itemPrice = num * p.Price;
        }

        public override bool Equals(object obj)
        {
            OrderItem o = obj as OrderItem;
            return o != null && p.Name == o.p.Name;
        }

        public override string ToString()
        {
            return "item 订单编号" + id + " 商品" + p + " 数量" + num + " 总价" + itemPrice;
        }




    }

    public class OrderService
    {
        private List<Order> orders;
        public List<Order> Orders { get; set; }

        public OrderService() { }
        public OrderService(List<Order> orders)
        {
            this.orders = orders;
        }
        //添加订单
        public void AddOrder(Order o)
        {
            if (!orders.Contains(o))
            {
                orders.Add(o);
                return;
            }
            Console.Write("订单重复输入");
            throw new Exception();
        }

        //删除订单
        public void DeleteOrder(Order o)
        {
            if (orders.Contains(o))
            {
                orders.Remove(o);
                return;
            }
            Console.WriteLine("表单中不存在此条订单，无法删除");
            throw new Exception();

        }

        //修改订单客户、地址、商品列表
        public void ChangeOrder(Order o,Client client)
        {
            int num = orders.IndexOf(o);
            o.Client = client;
            orders[num].Client = client;

        }
        public void ChangeOrder(Order o,string addr)
        {
            int num = orders.IndexOf(o);
            o.Address = addr;
            orders[num].Address = addr;
        }
        public void ChangeOrder(Order o,List<OrderItem > items)
        {
            int num = orders.IndexOf(o);
            o.Items = items;
            orders[num].Items = items;
        }

        //查询
        public IEnumerable<Order> SearchOrder(int opt,string info)
        {
            switch (opt)
            {
                case 0:
                    var query1 = from od in orders
                                 where od.OrderId == info
                                 orderby od.TotalPrice
                                 select od;
                    return query1;
                case 1:
                    var query2 = from od in orders
                                 where od.Client.ToString() == info
                                 orderby od.TotalPrice
                                 select od;
                    return query2;
                case 2:
                    var query3 = from od in orders
                                 from items in od.Items
                                 where items.P.Name == info
                                 select od;
                    return query3;
                case 3:
                    var query4 = from od in orders
                                 where od.TotalPrice.ToString() == info
                                 select od;
                    return query4;
                default:
                    Console.WriteLine("只可通过客户名、订单号、商品名、订单金额方式查询订单");
                    return null;

            }
        }

        public override string ToString()
        {
            string s = "";
            foreach(Order o in orders)
            {
                s += o + " ";
            }
            return s;
        }

        public void SortOrder()
        {
            orders = orders.OrderByDescending(o => o.OrderId).ToList();
        }
    }


    public class Product
    {
        private string id;
        private string name;
        private double price;

        public string Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public Product() { }

        public Product(string id,string name,double price)
        {
            this.id = id;
            this.name = name;
            this.price = price;
        }

        public override string ToString()
        {
            return "商品id：" + id + " 商品名：" + name + " 商品单价：" + price;
        }
    }

    public class Client
    {
        private string clinetId;

        public string ClientId { get; }
        public Client() { }
        public Client(string clientId)
        {
            this.clinetId = clientId;
        }
        public override string ToString()
        {
            return "账户号" + clinetId;
        }


    }

    public class OrderDetails
    {
        private string orderTime;
        private Order o;

        public string OrderTime { get; set; }
        public Order O { get; }

        public OrderDetails() { }
        public OrderDetails(string orderTime,Order o)
        {
            this.orderTime = orderTime;
            this.o = o;
        }
        public override string ToString()
        {
            return "订单时间：" + orderTime + o.ToString();
        }

        public override bool Equals(object obj)
        {
            OrderDetails od = obj as OrderDetails;
            return od != null && orderTime != od.orderTime && od.o.Equals(obj);
        }

    }

    [TestClass]
    public class test
    {
        Product p1, p2, p3, p4, p5;
        Client c1, c2, c3;
        OrderItem o1, o2, o3, o4;
        List<OrderItem> l1, l2, l3, l4;

        Order od1, od2, od3, od4;

        List<Order> orders;
        OrderService ods;

        public void init()
        {   //生成具体的商品 ——商品编号，商品名，商品单价
            p1 = new Product("001", "水杯", 15);
            p2 = new Product("002", "小刀", 10);
            p3 = new Product("003", "纸巾",3);
            p4 = new Product("004", "牙刷", 5);
            p5 = new Product("005", "茶叶",50);
            //生成顾客——顾客id
            c1 = new Client("c1");
            c2 = new Client("c2");
            c3 = new Client("c3");
            //生成某一商品的具体购买清单——订单号，数目，所购买的商品
            o1 = new OrderItem("o1", 3, p1);
            o2 = new OrderItem("o2", 1, p2);
            o3 = new OrderItem("o3", 10, p3);
            o4 = new OrderItem("o4", 2, p5);
            // 生成所有购买商品的List
            l1 = new List<OrderItem>();
            l2 = new List<OrderItem>();
            l3 = new List<OrderItem>();
            l4 = new List<OrderItem>();
            
            l1.Add(o1);
            l1.Add(o3);
            l2.Add(o2);
            l3.Add(o4);
            l3.Add(o1);
            l4.Add(o3);
            
            //生成订单——订单编号，地址，顾客，商品集
            od1 = new Order("od1", "addr1", c1, l1);
            od2 = new Order("od2", "addr1", c1, l2);
            od3 = new Order("od3", "addr2", c2, l3);
            od4 = new Order("od4", "addr3", c3, l4);

            orders = new List<Order>();
            ods = new OrderService(orders);
            ods.AddOrder(od1);
            ods.AddOrder(od2);
        }

        [TestMethod]
        public void AddOrderTest()
        {
            ods.AddOrder(od3);
            CollectionAssert.Contains(ods.Orders, od3);

        }

        [TestMethod]
        public void DeleteOrderTest()
        {
            ods.DeleteOrder(od1);
            CollectionAssert.Contains(ods.Orders, od1);

        }
        [TestMethod]
        public void SearchOrderTest()
        {
            IEnumerable<Order> result1 = ods.SearchOrder(0, "od1");
            result1.Contains(od1);
            IEnumerable<Order> result2 = ods.SearchOrder(1, "c1");
            result2.Contains(od1);
            result2.Contains(od2);
            IEnumerable<Order> result3 = ods.SearchOrder(2, "茶叶");
            result3.Contains(od3);
            IEnumerable<Order> result4 = ods.SearchOrder(3, "30");
            result4.Contains(od4);

        }
        [TestMethod]
        public void ChangeOrderTest()
        {
            ods.ChangeOrder(od1, c2);
            Assert.AreEqual(c2, od1.Client);

            ods.ChangeOrder(od1, "addr3");
            Assert.AreEqual("addr3", od1.Address);
        }
       

    }



}
