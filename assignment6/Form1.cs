﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OrderManagement;

namespace OrderManagementForm
{
    public partial class Form1 : Form
    {
        public List<Order> orders;
        //public OrderService ods;
        public int selectedIndex;
        public Order selectedOrder;

        public Form1()
        {
            InitializeComponent();

            orderBindingSource.DataSource = OrderService.GetAllOrders();
            cmbOpt.SelectedIndex = 0;
            //txtInfo.DataBindings.Add("Text", this, "Keyword");
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string sinfo = txtInfo.Text;

            orders = OrderService.SearchOrder(cmbOpt.SelectedIndex+1, sinfo).ToList();
            orderBindingSource.DataSource = orders;
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedIndex = e.RowIndex;
            selectedOrder = orders[selectedIndex];
            orderItemBindingSource.DataSource = selectedOrder.Items;
            orderItemBindingSource.ResetBindings(false);
        }

        private void btnAddOd_Click(object sender, EventArgs e)
        {

            Form2 f2 = new Form2(new Order());
            if (f2.ShowDialog() == DialogResult.OK)
            {
                OrderService.AddOrder(f2.CurrentOrder);
                orderBindingSource.DataSource = OrderService.GetAllOrders();
                orderBindingSource.ResetBindings(false);
            }
            //f2.Show();
        }
    }
}
