﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace 形状类
{
    internal class Program
    {   static Shape shape = null;
        static void Main(string[] args)
        {
            
            int number = 10;
            double sum = 0;
            for(int i = 0; i < number; i++)
            {
                Random romdom = new Random(Guid.NewGuid().GetHashCode());//防止循环中生成的随机数重复
                CreateShape(romdom);
                sum += shape.getArea();
            }
            Console.WriteLine("总面积是" + sum.ToString());
        }

        static Shape CreateShape(Random romdom)
        {
            
            int kind;
            kind = romdom.Next(1, 4);//生成1，2，3的随机数
            switch (kind)
            {
                case 1:
                    shape = new Rectangle(10 * romdom.NextDouble(), 10 * romdom.NextDouble());//生成一个0-10之间的浮点数
                    break;
                case 2:
                    shape = new Square(10 * romdom.NextDouble());
                    break;
                case 3:
                    bool tag = false;
                    while (tag == false)
                    {
                        shape = new Triangle(10 * romdom.NextDouble(), 10 * romdom.NextDouble(), 10 * romdom.NextDouble());
                        if (shape.IsLegal() == true)
                        {
                            tag = true;
                            Console.WriteLine("创建一个三角形");
                        }
                        else
                        {
                            Console.WriteLine("创建的三角形不满足三边关系");
                        }
                    }
                    break;
            }
            return shape;
        }
    }
}

//Shape类
public abstract class Shape 
{
    public abstract double getArea();
    public abstract bool IsLegal();

}

//长方形类
public class Rectangle : Shape 
{
    private double length;
    private double width;
    public double Length
    {
        get { return length; }
        set { length = value; }
    }
    public double Width
    {
        get { return width; }
        set { width = value; }
    }
    //长方形的面积计算
    public override double getArea()
    {
        return width * length;
    }
    public override bool IsLegal()
    {
            return false;
    }
    public Rectangle(double length,double width)
    {
        this.length = length;
        this.width = width;
        Console.WriteLine("创建一个长方形");
    }
}

//正方形
public class Square : Shape
{
    private double size;
    public double Size{
        get { return size; }
        set { size = value; }
    }
    public override double getArea()
    {
        return size * size;

    }
    public override bool IsLegal()
    {
          return false;
        
    }
    public Square(double size)
    {
        this.size = size;
        Console.WriteLine("创建一个正方形");

    }
}

//三角形
public class Triangle: Shape
{
    private double a;
    private double b;
    private double c;

    public double A
    {
        get { return a; }
        set { a = value; }
    }
    public double B
    {
        get { return b; }
        set { b = value; }
    }
    public double C
    {
        get { return c; }
        set { c = value; }
    }
    public override double getArea()//海伦公式计算三角形面积
    {
        double p = (a + b + c) / 2;
        return Math.Sqrt(p * (p - a) * (p - b) * (p - c));

    }
    public override bool IsLegal()
    {
        if(a+b<=c||a+c<=b||b+c<=a)
        {
            return false;
        }
        else { return true; }

    }
    public Triangle(double a,double b,double c)
    {
        this.a = a;
        this.b = b;
        this.c = c;
    }

}




