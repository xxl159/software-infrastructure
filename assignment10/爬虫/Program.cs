﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleCrawler
{
    class SimpleCrawler
    {
        private Hashtable urls = new Hashtable();
        private int count = 0;
        static async Task Main(string[] args)
        {
            SimpleCrawler myCrawler = new SimpleCrawler();
            string startUrl = "http://www.cnblogs.com/dstang2000/";
            if (args.Length >= 1) startUrl = args[0];
            myCrawler.urls.Add(startUrl, false);//加入初始页面
            await myCrawler.CrawlAsync();
        }

        private async Task CrawlAsync()
        {
            Console.WriteLine("开始爬行了.... ");
            List<Task> tasks = new List<Task>();
            while (count <= 10)
            {
                string current = null;
                lock (urls)
                {
                    current = urls.Keys.OfType<string>().FirstOrDefault(url => !(bool)urls[url]);
                    if (current != null) urls[current] = true;
                }

                if (current == null) break;

                Task task = Task.Run(async () => {
                    Console.WriteLine("爬行" + current + "页面!");
                    string html = await DownLoadAsync(current); // 下载
                    count++;
                    Parse(html);//解析,并加入新的链接
                    Console.WriteLine("爬行结束");
                });
                tasks.Add(task);
            }
            await Task.WhenAll(tasks);
        }

        public async Task<string> DownLoadAsync(string url)
        {
            try
            {
                WebClient webClient = new WebClient();
                webClient.Encoding = Encoding.UTF8;
                string html = await webClient.DownloadStringTaskAsync(url);
                string fileName = count.ToString();
                await File.WriteAllTextAsync(fileName, html, Encoding.UTF8);
                return html;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
        }

        private void Parse(string html)
        {
            string strRef = @"(href|HREF)[]*=[]*[""'][^""'#>]+[""']";
            MatchCollection matches = new Regex(strRef).Matches(html);
            foreach (Match match in matches)
            {
                strRef = match.Value.Substring(match.Value.IndexOf('=') + 1)
                          .Trim('"', '\"', '#', '>');
                if (strRef.Length == 0) continue;
                lock (urls)
                {
                    if (urls[strRef] == null) urls[strRef] = false;
                }
            }
        }
    }
}