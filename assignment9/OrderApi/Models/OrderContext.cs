﻿using Microsoft.EntityFrameworkCore;

namespace OrderApi.Models
{
    //[DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class OrderContext:DbContext
    {
        public OrderContext(DbContextOptions<OrderContext> options) : base(options)
        {
            this.Database.EnsureCreated();
        }
        
       /* public OrderContext() : base("OrderDataBase")
        {
            Database.SetInitializer(
               new DropCreateDatabaseIfModelChanges<OrderContext>());
        }*/

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<OrderDetails> OrderDetails { get; set; }



    }
}
