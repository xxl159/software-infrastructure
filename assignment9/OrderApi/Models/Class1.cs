﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;    

namespace OrderApi.Models

{
    
    public class Order
    {
        public string s;
        public string OrderID { get; set; }
        public string Address { get; set; }
        public double TotalPrice 
        { set {
                foreach (OrderItem item in Items)
                {
                    TotalPrice += item.ItemPrice;
                }
            }
            get { return TotalPrice; }
            
            }
        public List<OrderItem> Items { get;set;}
        public Client Client { get; set; }
        public override string ToString()
        {
            string s = "订单ID" + OrderID + "  订单地址：" + Address + "  客户名：" + Client + "  商品总价：" + TotalPrice;
            foreach (OrderItem item in Items)
            {
                s += item + " ";
            }
            return s;
        }


    }
    public class OrderItem
    {
        public string OrderItemID { get; set; }
        public int Num { get; set; }
        public Product P { get; set; }
        public double ItemPrice
        { 
            get { return P.SinglePrice*Num; }
            
        }
        public override string ToString()
        {
            return "item 订单编号" + P.ProductID + " 商品名" + P.Name + " 数量" + Num + " 总价" + ItemPrice;
        }

    }

    public class Product
    {
        public string ProductID { get; set; }
        public string Name { get; set; }
        public double SinglePrice
        {
            get { return SinglePrice; }
            set
            {
                if (value < 0)
                {
                    throw new Exception("商品价格不可为负！");
                }
                else
                    SinglePrice = value;

            }
        }
    }

    public class Client
    {
        public string ClientID { get; set; }

    }

    public class OrderDetails
    {
        public string OrderTime { get; set; }
        public Order O { get; set; }

    }
}
