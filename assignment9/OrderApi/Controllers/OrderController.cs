using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using OrderApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;
using System.Net;

namespace OrderApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class OrderController : ControllerBase
{
    private readonly OrderContext orderDb;

    public OrderController(OrderContext context)
    {
        this.orderDb = context;
    }

    [HttpGet("{id}")]
    public ActionResult<Order> GetOrder(string id)
    {
        var or = orderDb.Orders.FirstOrDefault(o => o.OrderID == id);
        if (or == null)
        {
            return NotFound();
        }
        return or;
    }

    [HttpGet]
    public ActionResult<List<Order>> GetOrders(string? address, Client? client)
    {
        var query = buildQuery(address, client);
        return query.ToList();

    }

    [HttpGet("pageQuery")]
    public ActionResult<List<Order>> queryOrder(string? address, Client? client, int skip, int take)
    {
        var query = buildQuery(address, client).Skip(skip).Take(take);
        return query.ToList();

    }

    private IQueryable<Order> buildQuery(string? address, Client? client)
    {
        IQueryable<Order> query = orderDb.Orders;
        if (address != null)
        {
            query = query.Where(t => t.Address.Contains(address));
        }
        if (client != null)
        {
            query = query.Where(t => t.Client == client);
        }
        return query;
    }


    [HttpPost]
    public ActionResult<Order> PostOrder(Order or)
    {
        try
        {
            orderDb.Orders.Add(or);
            orderDb.SaveChanges();
        }catch(Exception e)
        {
            String error = (e.InnerException == null) ? e.Message
                : e.InnerException.Message;
            return BadRequest(error);
        }
        return or;
    }

    [HttpPut("{id}")]
    public ActionResult<Order> PutOrder(string id, Order or)
    {
        if (id != or.OrderID)
        {
            return BadRequest("OrderId cannot be modified!");
        }
        try
        {
            orderDb.Entry(or).State = EntityState.Modified;
            orderDb.SaveChanges();
        }catch(Exception e)
        {
            String error = (e.InnerException == null) ? e.Message
                : e.InnerException.Message;
            return BadRequest(error);
        }
        return NoContent();
    }

    [HttpDelete("{id}")]
    public ActionResult DeleteOrder(string id)
    {
        try
        {
            var or = orderDb.Orders.FirstOrDefault(t => t.OrderID == id);
            if (or != null)
            {
                orderDb.Orders.Remove(or);
                orderDb.SaveChanges();
            }
        }catch(Exception e)
        {
            String error = (e.InnerException == null) ? e.Message
                : e.InnerException.Message;
            return BadRequest(error);
        }
        return BadRequest();
    }
}
