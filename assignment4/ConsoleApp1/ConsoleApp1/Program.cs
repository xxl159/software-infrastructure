﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   
   class Node<T>
    {
        public Node<T> Next { get; set; }
        public T Data { get; set; }

        public Node(T t)
        {
            Next = null;
            Data = t;
        }
    }
    class GenericList<T>
    {
        private Node<T> head;
        private Node<T> tail;

        public GenericList()
        {
            tail = head = null;
        }

        public Node<T> Head
        {
            get => head;

        }
        public void Add(T t)
        {
            Node<T> n = new Node<T>(t);
            if (tail == null)
            {
                head = tail = n;
            }
            else
            {
                tail.Next = n;
                tail = n;
            }
        }

        public void ForEach(Action<T> action)
        {
            Node<T> current = head;
            while (current != null)
            {
                action(current.Data);
                current = current.Next;
            }
        }
    }
        class Test
        {
            static void Main()
            {
                GenericList<int> intlist = new GenericList<int>();
                for (int x = 0; x < 10; x++)
                {
                    intlist.Add(x);
                }

                //打印链表元素
                intlist.ForEach(x => Console.Write(x + " "));
                //
                int max = 0;
                intlist.ForEach(x => { if (x > max) max = x; });
                Console.WriteLine("\nmax:" + max);

                //
                int min = 0;
                intlist.ForEach(x => { if (x < min) min = x; });
                Console.WriteLine("\nMin:" + min);

                int sum = 0;
                intlist.ForEach(x => sum += x);
                Console.WriteLine("\nsum:" + sum);


            }
        }
    
}
