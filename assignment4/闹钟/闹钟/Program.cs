﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Data;

namespace 闹钟
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Clock clock = new Clock();
            DateTime datatime = new DateTime();
            clock.RunClock();

        }
        
    }

    public class Clock
    {
        public event TickEventHandler Tick;
        public event AlarmEventHandler Alarm;

        public delegate void TickEventHandler(object sender, DateTime args);
        public delegate void AlarmEventHandler(object sender, DateTime args);

        DateTime alarmTime = DateTime.Now;
        

        

        public Clock()
        {
            Alarm += RunAlarm;
            Tick = RunTick;

        }

        public void RunAlarm(object sender,DateTime time)
        {
            Console.WriteLine("Alarm!!! It's "+time);
        }
        
        public void RunTick(object sender, DateTime time)
        {
            Console.WriteLine("now time:" + time);
        }

        public void RunClock()
        {
            Console.WriteLine("Alarm tick started.");
            while (true)
            {
                DateTime now = DateTime.Now;
                RunTick(this, now);
                if (now.ToString() == alarmTime.ToString())
                {
                    RunAlarm(this, alarmTime);
                }
                Thread.Sleep(1000);
               

            }
        }

        public void SetAlarmTime(DateTime alarmtime)
        {
            Console.WriteLine(alarmtime);
            this.alarmTime = alarmtime;

        }
    }
}
