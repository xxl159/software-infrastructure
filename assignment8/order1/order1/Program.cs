﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Validation;
using System.ComponentModel.DataAnnotations;
using Org.BouncyCastle.Asn1;
using System.Runtime.Remoting.Contexts;

namespace order1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //添加订单1
            using (var or = new OrderContext()) 
            {
                var p1 = new Product { ProductID = "p001", Name = "水杯", SinglePrice = 15 } ;
                var p2 = new Product { ProductID = "p002", Name = "小刀 ", SinglePrice = 10 };
                var client1 = new Client { ClientID = "c1" };
                
                var orderitem1 = new OrderItem { OrderItemID = "oi1", Num = 3, P = p1 };
                var orderitem2 = new OrderItem { OrderItemID = "oi2", Num = 1, P = p2 };
                var itemList1 = new List<OrderItem>();
                itemList1.Add(orderitem1);
                itemList1.Add(orderitem2);
                var order1 = new Order { OrderID="od1", Address="addr1" ,Client=client1,Items = itemList1};
                or.Orders.Add(order1);
                or.SaveChanges();
                
            }
            //添加订单2
            using (var or = new OrderContext())
            {
                var p3 = new Product { ProductID = "p003", Name = "纸巾", SinglePrice = 3 };
                var client2 = new Client { ClientID = "c2" };
                var orderitem3 = new OrderItem { OrderItemID = "oi3", Num = 10, P = p3 };
                var itemList2 = new List<OrderItem>();
                itemList2.Add(orderitem3);
                var order2 = new Order { OrderID = "od2", Address = "addr2", Client = client2, Items = itemList2 };
                or.Orders.Add(order2);
                or.SaveChanges();
            }
            //删除订单号为od1的订单
            using (var context = new OrderContext())
            {
                var order = context.Orders.Include("OrderItem").Include("Client").FirstOrDefault(p => p.OrderID == "od2");
                if (order != null)
                {
                    context.Orders.Remove(order);
                    context.SaveChanges();
                }

            }
            //将订单地址为addr1的订单地址改为newAddr
            using (var context = new OrderContext())
            {
                var order = context.Orders.FirstOrDefault(p => p.Address == "addr1");
                if (order != null)
                {
                    order.Address = "newAddr";
                    context.SaveChanges();
                }
            }
            //修改订单客户账号
            using (var context = new OrderContext())
            {
                var order = context.Orders.Include("Client").FirstOrDefault(P => P.OrderID == "od1");
                if (order != null)
                {
                    order.Client.ClientID = "newClient";
                    context.SaveChanges();
                }
            }

            //通过订单号进行查询
            using (var context = new OrderContext())
            {
                var order = context.Orders.Include("Client").Include("OrderItem").SingleOrDefault(p => p.OrderID == "od1");
                if (order != null)
                {
                    Console.WriteLine("编号od1的订单" + order.ToString());
                }
            }


        }
    }


}
